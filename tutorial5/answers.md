So in CraftServiceImpl.java, there are 5 methods that can be run one of the time.
When you make one of the items, it will mix all of the materials first
then compose it and finally displayed the result. This process too much time for just making one item
while we can make other items. For this problem, I import CompletableFuture to solve it.

For each method of createItems, I implemented runAsync for each recipes and insert composeRecipe inside each method.
This way, the process to create an item will store to a thread and it will run independently from other methods.
With this, we can create many items at once.

Since each method use runAsync and look alike, I intended to make another method that just run runAsync.
But I failed to implement it since I haven't understand how to make runAsync on loop.

