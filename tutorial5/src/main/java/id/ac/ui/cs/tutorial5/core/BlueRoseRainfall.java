package id.ac.ui.cs.tutorial5.core;

import id.ac.ui.cs.tutorial5.service.RandomizerService;

public class BlueRoseRainfall extends Craftable {

    public BlueRoseRainfall() {
        super("Blue Rose Rainfall", RandomizerService.getRandomCostValue());
    }

    @Override
    public String getDoneCraftMessage() {
        return "The rain comes from the deep sky, the water form the shape of Blue Roses \n";
    }
}
