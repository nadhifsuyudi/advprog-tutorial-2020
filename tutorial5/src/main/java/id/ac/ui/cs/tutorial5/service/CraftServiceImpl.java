package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CraftServiceImpl implements CraftService {

    private String[] itemName = {
            itemName1, itemName2, itemName3,
            itemName4, itemName5
    };
    private static final String itemName1 = "Dragon Breath";
    private static final String itemName2 = "Void Rhapsody";
    private static final String itemName3 = "Determination Symphony";
    private static final String itemName4 = "Opera of Wasteland";
    private static final String itemName5 = "FIRE BIRD";

    private  List<CraftItem> allItems = new ArrayList<>();

    @Override
    public CraftItem createItem(String itemName) {
        CraftItem craftItem;
        switch (itemName) {
            case itemName1:
                craftItem = createDragonBreath();
                break;
            case itemName2:
                craftItem = createVoidRhapsody();
                break;
            case itemName3:
                craftItem = createDeterminationSymphony();
                break;
            case itemName4:
                craftItem = createOperaOfWasteland();
                break;
            case itemName5:
                craftItem = createFireBird();
                break;
            default:
                craftItem = createFireBird();
        }
        allItems.add(craftItem);
        return craftItem;
    }

    private CraftItem createDragonBreath() {
        CraftItem dragonBreath = new CraftItem("Dragon Breath");
        CompletableFuture.runAsync(() -> dragonBreath.addRecipes(new SilentField()))
                .thenRunAsync(() -> dragonBreath.addRecipes(new FireCrystal()))
                .thenRun(dragonBreath::composeRecipes);
        return dragonBreath;
    }

    private CraftItem createVoidRhapsody() {
        CraftItem voidRhapsody = new CraftItem("Void Rhapsody");
        CompletableFuture.runAsync(() -> voidRhapsody.addRecipes(new SilentField()))
                .thenRunAsync(() -> voidRhapsody.addRecipes(new PureWaterfall()))
                .thenRun(voidRhapsody::composeRecipes);
        return voidRhapsody;
    }

    private CraftItem createDeterminationSymphony() {
        CraftItem determinationSymphony = new CraftItem("Determination Symphony");
        CompletableFuture.runAsync(() -> determinationSymphony.addRecipes(new BlueRoseRainfall()))
                .thenRunAsync(() -> determinationSymphony.addRecipes(new PureWaterfall()))
                .thenRun(determinationSymphony::composeRecipes);
        return determinationSymphony;
    }

    private CraftItem createOperaOfWasteland() {
        CraftItem wasteland = new CraftItem("Opera of Wasteland");
        CompletableFuture.runAsync(() -> wasteland.addRecipes(new DeathSword()))
                .thenRunAsync(() -> wasteland.addRecipes(new SilentField()))
                .thenRunAsync(() -> wasteland.addRecipes(new FireCrystal()))
                .thenRun(wasteland::composeRecipes);
        return wasteland;
    }

    private CraftItem createFireBird() {
        CraftItem fireBird =  new CraftItem("FIRE BIRD");
        CompletableFuture.runAsync(() -> fireBird.addRecipes(new FireCrystal()))
                .thenRunAsync(() -> fireBird.addRecipes(new BirdEggs()))
                .thenRun(fireBird::composeRecipes);
        return fireBird;
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
