package id.ac.ui.cs.tutorial5.core;

import id.ac.ui.cs.tutorial5.service.RandomizerService;

public class FireCrystal extends Craftable {

    public FireCrystal() {
        super("Fire Crystal", RandomizerService.getRandomCostValue());
    }

    @Override
    public String getDoneCraftMessage() {
        return "Red on the show, now you obtain the fire in the deep black \n";
    }
}
