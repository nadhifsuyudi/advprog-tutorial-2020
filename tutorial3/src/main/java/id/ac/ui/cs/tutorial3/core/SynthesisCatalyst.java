package id.ac.ui.cs.tutorial3.core;

public class SynthesisCatalyst extends Synthesis {
	public SynthesisCatalyst(String name, int mana){
		super(name, -1, mana);
	}

	public boolean respond(){
		boolean res = false;;
		if(this.mana >= -this.manaGain){
			this.mana = this.mana + this.manaGain;
			res = true;
		}
		return res;
	}

	public String requestMessage(){
		return "Synthesis: " + this.name + " - Supplying Mana " + (-this.manaGain);
	}
}