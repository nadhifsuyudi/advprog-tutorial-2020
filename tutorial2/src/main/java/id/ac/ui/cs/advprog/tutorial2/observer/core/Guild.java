package id.ac.ui.cs.advprog.tutorial2.observer.core;

import java.util.ArrayList;
import java.util.List;

public class Guild {
        private List<Adventurer> adventurers = new ArrayList<>();
        private Quest quest;

        public void add(Adventurer adventurer) {
                adventurers.add(adventurer);
        }

        //Todo: Something seems redundant here
        public void addQuest(Quest quest) {
                this.quest = quest;
                for (Adventurer adventurer: adventurers) {
                        adventurer.update();
                }
        }

        public void broadcastQuest(Quest quest) {
                addQuest(quest);
        }

        public String getQuestType () {return quest.getType();}

        public Quest getQuest() {return quest;}

        public List<Adventurer> getAdventurers() {
                return adventurers;
        }
}
