package id.ac.ui.cs.advprog.tutorial2.observer.service;

import id.ac.ui.cs.advprog.tutorial2.observer.core.*;
import id.ac.ui.cs.advprog.tutorial2.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial2.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                Adventurer agileAdventurer = new AgileAdventurer("Agile", guild);
                Adventurer knightAdventurer = new KnightAdventurer("Knight", guild);
                Adventurer mysticAdventurer = new MysticAdventurer("Mystic", guild);
        }

        @Override
        public void addQuest(Quest quest) {
                questRepository.save(quest);
                this.guild.broadcastQuest(quest);
        }

        @Override
        public List<Adventurer> getAdventurers() {
                return guild.getAdventurers();
        }
}
