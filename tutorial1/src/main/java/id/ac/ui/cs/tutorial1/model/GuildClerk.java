package id.ac.ui.cs.tutorial1.model;

import java.util.Date;

public class GuildClerk extends GuildEmployee {

    public GuildClerk(String name, String familyName, String id, Date birthDate) {
        super(name, familyName, id, birthDate);
    }

    @Override
    public String getWork() {
        return "Guild Clerk";
    }
}
