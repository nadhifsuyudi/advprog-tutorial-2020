package id.ac.ui.cs.tutorial4.repository;

import java.util.List;

import id.ac.ui.cs.tutorial4.model.Conference;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ConferenceRepo
 */
public interface ConferenceRepo extends JpaRepository<Conference, Long> {
    List<Conference> findByNama(String nama);
    List<Conference> findAllByOrderByIdAsc();
}